//Name: Hee Yun Suh  Chanha Kim
//JHED: hsuh11       ckim135
// ppm_io.c

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>
#include "ppm_io.h"

/* Read a PPM-formatted image from a file (assumes fp != NULL).
 * Returns the address of the heap-allocated Image struct it
 * creates and populates with the Image data.
 */
Image * read_ppm(FILE *fp) {
  // check that fp is not NULL
  assert(fp);

  //initialize
  char tag[10];
  int col;
  int row;
  int colors;

  //read tag
  if (fscanf(fp, " %s", tag) != 1) {
    fprintf(stderr, "Error: invalid tag.\n");
    return NULL;
  } else if (strcmp(tag, "P6") != 0) {
    fprintf(stderr, "Error: invalid tag.\n");
    return NULL;
  }
  
  
  //read comments if there are any
  char c;
  fscanf(fp, " %c", &c);
  while (c == '#') {
    while (getc(fp) != '\n');
    c = getc(fp);
  }
  ungetc(c, fp);

  //get rows and columns and max color value
  if (fscanf(fp, "%d %d %d", &col, &row, &colors) != 3) {
    fprintf(stderr, "Error: invalid col, row, or maxval.\n");
    return NULL;
  } else if (col < 0 || row < 0) {
    fprintf(stderr, "Error: negative row, or col.\n");
    return NULL;
  }
  
  //if colors is not 255, reject
  if (colors != 255) {
    fprintf(stderr, "Error: Invalid colors in file.\n");
    return NULL;
  }

  //skip through empty white spaces until the pixel value
  while(fgetc(fp) != '\n');
  
 
  //heap-allocated array of pixels
  Pixel *pixel_array = malloc(sizeof(Pixel)*row*col);
  
  //read pixels
  size_t num_elements = fread(pixel_array, 3 * col, row, fp);
  if ((int)num_elements != (row)) {
    fprintf(stderr, "Error: Problem reading data.%ld\n", num_elements);
    free(pixel_array);
    return NULL;
  }

  if (feof(fp)) {
    fprintf(stderr, "Error: Unexpected EOF.\n");
    free(pixel_array);
    return NULL;
  }

  if (ferror(fp)) {
    fprintf(stderr, "Error: Error reading pixels.\n");
    free(pixel_array);
    return NULL;
  }

  //create Image struct and put array of pixels into the data pointer
  Image *read_image = (Image *)malloc((int)sizeof(Image));
  read_image->data = pixel_array;
  read_image->rows = row;
  read_image->cols = col;

  fclose(fp);
  return read_image;  
}

/* Write a PPM-formatted image to a file (assumes fp != NULL),
 * and return the number of pixels successfully written.
 */
  
int write_ppm(FILE *fp, const Image *im) {

  // check that fp is not NULL
  assert(fp);
  

  // write PPM file header, in the following format
  // P6
  // cols rows
  // 255
  fprintf(fp, "P6\n%d %d\n255\n", im->cols, im->rows);

  // now write the pixel array
  int num_pixels_written = fwrite(im->data, sizeof(Pixel), im->cols * im->rows, fp);

  if (num_pixels_written != im->cols * im->rows) {
    fprintf(stderr, "Uh oh. Pixel data failed to write properly!\n");
    return 7;
  }

  return num_pixels_written;
}

/* Copies Image struct.
 * Returns pointer to copied Image.
 * DOES NOT FREE the input_image
 */
Image * copy_image(Image * input_image){
  //create new Image struct
  Image * copy = malloc(sizeof(Image));
  copy->data = malloc(sizeof(Pixel) * input_image->rows
		      * input_image->cols);
  copy->rows = input_image->rows;
  copy->cols = input_image->cols;
  for(int i = 0; i < (copy->rows * copy->cols); i++){
    copy->data[i].r = input_image->data[i].r;
    copy->data[i].g = input_image->data[i].g;
    copy->data[i].b = input_image->data[i].b;
  }

  return copy;
}


/* Destroy Image struct and frees all memory allocated for image and its pixel data.
 */
void destroy_image(Image * input_image){
  //first free data then image
  if (input_image->data != NULL){
    free(input_image->data);
    input_image->data = NULL;
  }
  free(input_image);
}

/* Copies Pixel struct from first input into
 * the second input pointer..
 */
void copy_pixel(Pixel * input_pix1, Pixel * input_pix2){
  input_pix2->r = input_pix1->r;
  input_pix2->g = input_pix1->g;
  input_pix2->b = input_pix1->b;
}

  
  
    











  

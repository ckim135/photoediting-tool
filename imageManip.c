//Chanha Kim / Hee Yun Suh
//ckim135 / hsuh11

//imagemanip.c
#include <stdio.h>
#include <stdlib.h>
#include "imageManip.h"
#include "ppm_io.h"
#include <math.h>

//changes exposure to newly created image struct
Image * exposure(Image * input_image, float EV){
  
  //for each pixel
  Pixel new;
  int red;
  int blue;
  int green;
  // loop through and calculate
  for (int r = 0; r < input_image->rows; r++) {
    for (int c = 0; c < input_image->cols; c++) {
      //new pixel thats modified version of original
      new = input_image->data[r * input_image->cols + c];
      red = (int)(new.r * pow(2,EV));
      if (red > 255) {
	new.r = 255;
      } else {
	new.r = red;
      }
      blue = (int)(new.b * pow(2,EV));
      if (blue > 255) {
	new.b = 255;
      } else {
	new.b = blue;
      }
      green = (int)(new.g * pow(2,EV));
      if (green > 255) {
	new.g = 255;
      } else {
	new.g = green;
      }
      input_image->data[r * input_image->cols + c] = new;
    }
  }

  return input_image;
}

//blends two image structs togeter to create new image
Image * blend (Image * input_image1, Image * input_image2, float a){
  //initialize variables
  int row_large;
  int row_small;
  int col_large;
  int col_small;;
  //check dimensions of input images
  if (input_image1->rows > input_image2->rows){
    row_large = input_image1->rows;
    row_small = input_image2->rows;
  } else {
    row_large = input_image2->rows;
    row_small = input_image1->rows;
  }

  if (input_image1->cols > input_image2->cols){
    col_large = input_image1->cols;
    col_small = input_image2->cols;
  } else {
    col_large = input_image2->cols;
    col_small = input_image1->cols;
  }
  //new Image struct
  Image * new_im = malloc(sizeof(Image));  
  new_im->data = malloc(sizeof(Pixel)*row_large*col_large);
  new_im->rows = row_large;
  new_im->cols = col_large;
  Pixel * data_1 = input_image1->data;
  Pixel * data_2 = input_image2->data;
  //calculate
  for (int i = 0; i < col_large; i++){
    for (int j = 0; j < row_large; j++){
      //calculate new pixel for overlapped part
      if (i < col_small){
	if (j < row_small){
	  new_im->data[j*col_large+i].r = a*data_1[j*input_image1->cols+i].r
	    +(1-a)*data_2[j*input_image2->cols+i].r;
	  new_im->data[j*col_large+i].g = a*data_1[j*input_image1->cols+i].g
	    +(1-a)*data_2[j*input_image2->cols+i].g;
	  new_im->data[j*col_large+i].b = a*data_1[j*input_image1->cols+i].b
	    +(1-a)*data_2[j*input_image2->cols+i].b;
	} else {
	  //rest of rows follow image that has more rows
	  if (row_large > input_image1->rows){
	    copy_pixel(&(data_2[j*input_image2->cols+i]),
		       &(new_im->data[j*col_large+i]));
	  } else {
	    copy_pixel(&(data_1[j*input_image1->cols+i]),
		       &(new_im->data[j*col_large+i]));
	  }
	}
      } else {
	//rest of cols follow image with more columns
	if (j < row_small){
	  if (col_large > input_image1->cols){
	    copy_pixel(&(data_2[j*input_image2->cols+i]),
		       &(new_im->data[j*col_large+i]));
	  } else {
	    copy_pixel(&(data_1[j*input_image1->cols+i]),
		       &(new_im->data[j*col_large+i]));
	  }
	} else {
	  //place with no pixels is automatically 0
	  new_im->data[j*col_large+i].r = 0;
	  new_im->data[j*col_large+i].g = 0;
	  new_im->data[j*col_large+i].b = 0;
	}
      }
    }
  }
  destroy_image(input_image1);
  destroy_image(input_image2);

  return new_im;
}

//function create zoomed in image struct
Image * zoom_in (Image * im) {
  //initialize Image struct
  Image * copy = copy_image(im);
  copy->rows = 2 * copy->rows;
  copy->cols = 2 * copy->cols;
  copy->data = realloc(copy->data, sizeof(Pixel) * im->rows * im->cols * 4);
  //loop through and copy each pixel four times into copy data
  for (int r = 0; r < copy->rows; r = r + 2) {
    for (int c = 0; c < copy->cols; c = c + 2) {
      copy->data[r * copy->cols + c] = im->data[(r/2) * im->cols + (c/2)];
      copy->data[r * copy->cols + c + 1] = im->data[(r/2) * im->cols + (c/2)];
      copy->data[(r+1) * copy->cols + c] = im->data[(r/2) * im->cols + (c/2)];
      copy->data[(r+1) * copy->cols + c + 1] = im->data[(r/2) * im->cols + (c/2)];
    }
  }

  free(im->data);
  free(im);
  return copy;    
}

//function creates zoomed out image struct based off original image
Image * zoom_out (Image * input_image){
  int pix1;
  int pix2;
  int pix3;
  int pix4;
  //create Image struct
  Image * new_image = malloc(sizeof(Image));
  new_image->data = malloc(sizeof(Pixel)*
			   input_image->rows
			   *input_image->cols/4);
  //add colors to data
  for (int i = 0; i < input_image->rows/2; i++){
    for (int j = 0; j < input_image->cols/2; j++){
      //find the four coordinates that are next to each other
      pix1 = (i*2)*input_image->cols+(j*2);
      pix2 = (i*2)*input_image->cols+(j*2+1);
      pix3 = (i*2+1)*input_image->cols+(j*2);
      pix4 = (i*2+1)*input_image->cols+(j*2+1);
      //average colors
      new_image->data[i*(input_image->cols/2)+j].r =
	(input_image->data[pix1].r +
	 input_image->data[pix2].r +
         input_image->data[pix3].r +
	 input_image->data[pix4].r)/4;
      new_image->data[i*(input_image->cols/2)+j].g =
	(input_image->data[pix1].g +
	 input_image->data[pix2].g +
	 input_image->data[pix3].g +
	 input_image->data[pix4].g)/4;
      new_image->data[i*(input_image->cols/2)+j].b =
	(input_image->data[pix1].b +
	 input_image->data[pix2].b +
	 input_image->data[pix3].b +
	 input_image->data[pix4].b)/4;
    }
  }
  //add rows and cols to new_image
  new_image->rows = input_image->rows/2;
  new_image->cols = input_image->cols/2;
  //return pointer
  destroy_image(input_image);
  return new_image;
}

//function creates artistic dotted image struct based off
//original image
Image * pointilism (Image * im) {
  for (int i = 0; i < (int)(im->rows * im->cols * 0.03); i++) {
    int x = rand() % im->cols; //random x coordinate of center
    int y = rand() % im->rows; //random y coordinate of center
    int r = (rand() % 5) + 1; //random radius
    
    for (int yc = y-r; yc < y+r+1; yc++) {
      if (yc > (im->rows)-1 || yc < 0) {
	continue;
      }
      for (int xc = x-r; xc < x+r+1; xc++) {
	if (xc > (im->cols)-1 || xc < 0) {
	  continue;
	}
	if (pow((x-xc) , 2) + pow((y-yc) , 2) <= pow(r , 2)) {
	  im->data[yc * im->cols + xc] = im->data[y * im->cols + x];
	}
      }
    }
  }
  return im;
}

//function creates image struct with swirled image of input image
Image * swirl (Image * input_image, int cx, int cy, int ds){
  //new Image struct
  Image * new_image = malloc(sizeof(Image));
  new_image->data = malloc(sizeof(Pixel)*input_image->rows*input_image->cols);
  new_image->rows = input_image->rows;
  new_image->cols = input_image->cols;
  int col = input_image->cols;
  Pixel * data = new_image->data;
  double a;
  int i, j;
  //move Pixels to new location
  for (int x = 0; x < col; x++){
    for (int y = 0; y < input_image->rows; y++){
      a = sqrt(pow(x-cx,2)+pow(y-cy,2))/ds;
      i = (int)((x-cx)*cos(a)-(y-cy)*sin(a)+cx);
      j = (int)((x-cx)*sin(a)+(y-cy)*cos(a)+cy);
      if (i < 0 || i >= col || j < 0 || j >= input_image->rows){
	data[y*col+x].r = 0;
	data[y*col+x].g = 0;
	data[y*col+x].b = 0;
      } else {
	copy_pixel(&(input_image->data[j*col+i]),&(data[y*col+x]));
      }
    }
  }
  //return pointer
  destroy_image(input_image);
  return new_image;  
}


//Blur function: Gaussian blur
Image * blur (Image * input_image, float sigma) {
  int N = 10 * sigma; //dimension N
  if (N % 2 == 0) { //if N is even add one
    N++;
  }
  double ** g_filter = filter(sigma, N); //make gaussian filter

  Image * copy = copy_image(input_image);

  //loop through data and copy pixel for each coordinate
  for (int i = 0; i < input_image->rows; i++) {
    for (int j = 0; j < input_image->cols; j++) {
      copy->data[i * copy->cols + j] = filter_response(input_image, j, i, g_filter, N);
    }
  }
  //free filter and image
  for (int i = 0; i < N; i++) {
    free(g_filter[i]);
  }
  free(g_filter);
  free(input_image->data);
  free(input_image);
  return copy;
}

//Gaussian filter populator, returns pointer to filter matrix
double ** filter (float sigma, int N) {
  double ** g_filter = malloc(sizeof(double*) * N); //dynamically allocate 2D array of floats
  for (int i = 0; i < N; i++) {
    g_filter[i] = malloc(sizeof(double) * N);
  }
  //Note that center is always N/2
  int dx;
  int dy;
  double g;
  for (int j = 0; j < N; j++) { //jth row
    for (int k = 0; k < N; k++) { //kth column
      dx = k - N/2;
      dy = j - N/2;
      g = (1.0 / (2.0) * PI * pow(sigma,2)) * exp( -(pow(dx,2) + pow(dy,2)) / (2 * pow(sigma,2)));
      g_filter[j][k] = g;
    }
  }
  return g_filter;
}

//filter_response for each pixel
Pixel filter_response (Image * input_image, int x, int y,  double ** filter, int N){
  //initialize variables
  Pixel output;
  double r = 0;
  double g = 0;
  double b = 0;
  double sum_filter = 0;
  //calculate filter portion that overlaps with original image
  int i = (int)fmax(0, (int)(y - N/2));
  int j;
  int k = (int)fmin(input_image->rows, (int)(y + N/2 + 1));
  int l = (int)fmin(input_image->cols, (int)(N/2 + x + 1));
  //calculate starting values in the filter array
  int fil_x = (int)fmax((int)(N/2 - x), 0);
  int fil_y = (int)fmax((int)(N/2 - y), 0);
  int coor;
  //sum weighted values and filter values;
  for (i = i; i < k; i++){
    for (j = (int)fmax(0, (int)(x - N/2)); j < l; j++){
      coor = i*input_image->cols+j;
      r += filter[fil_y][fil_x]*input_image->data[coor].r;
      g += filter[fil_y][fil_x]*input_image->data[coor].g;
      b += filter[fil_y][fil_x]*input_image->data[coor].b;
      sum_filter += filter[fil_y][fil_x];
      fil_x++;
    }
    //increment y and reset x
    fil_y++;
    fil_x = (int)fmax((int)(N/2 - x), 0);
  }
  //add values to Pixel struct
  output.r = (int)(r/sum_filter);
  output.g = (int)(g/sum_filter);
  output.b = (int)(b/sum_filter);
  return output;
  
}

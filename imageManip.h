//Chanha Kim / Hee Yun Suh
//ckim135 / hsuh11

//imageManip.h

#ifndef IMAGEMANIP_H
#define IMAGEMANIP_H

#include <stdio.h>
#include "ppm_io.h"

#define PI     3.14159265358979323846
#define sq(x)  ((x)*(x))
//changing exposure function (need math.h header)
//if the new pixel color channel value exceeds 255, cap it at 255
//@param input_image: pointer to the input image struct
//@param EV: exposure value
//@return: pointer to the result image struct
Image * exposure(Image * input_image, float EV);

//a-Blending function blends two images into one using a given blend ratio alpha
//output = alpha * input1 + (1 - alpha) * input2
//if the dimensions images are different, result image has greater dimension of the two input
//@param image1, image2: two pointers to each image
//@param a: alpha
//@return: pointer to the result image struct 
Image * blend (Image * input_image1, Image * input_image2, float a);

//Zoom-in function
//fixed zoom in scale of 2x2 square of pixels
//zoomed in image has 2 times more rows and columns
//@param input_image: image pointer that will be zoomed in
//@return: pointer to zoomed in image struct
Image * zoom_in (Image * input_image);

//Zome-out function
//reverse of zoom in
//take 2x2 square of pixels of input_image and average three color channels of
//all four pixels to make a single pixel with each averaged color channel
//if input image is odd we lose bottom row or/and rightmost coloumn
//@param input_image: image pointer that will be zoomed out
//@return: pointer to zoomed out image struct
Image * zoom_out (Image * input_image);

//randomly select pixel and create circles with small radius to create
//a artistic dotted image.
//@param: pointer to Image struct that will be used for the effect
//@return: pointer to Image struct that has been changed.
Image * pointilism (Image * input_image);

//Image warping - swirl effect
//only changes the location of some or all pixels
//@param cx: column coordinate of center
//@parm cy: row coordinate of center
//@param ds: distortion scale
//@return: pointer to Image struct that is swirled
Image * swirl (Image * input_image, int cx, int cy, int ds);


//Blur function: Gaussian blur
//blurs image using the gaussian blur
//@param input_image: pointer to image struct that will be blurred
//@param sigma: sigma value used to make filter
//@return: pointer to blurred image
Image * blur (Image * input_image, float sigma);

//Gaussian filter populator
//@param sigma: value used to calculate filter
//@param N: width of filter
//@return: pointer to filter
double ** filter (float sigma, int N);

//filter_response for each pixel
//@param input_image: pointer to original image
//@param x,y: coordinates of pixel in orig image
//@param filter: pointer to filter
//@param N: width of filter
//@return: Pixel struct with newly calculated color channels
Pixel filter_response (Image * input_image, int x, int y,  double ** filter, int N);

#endif

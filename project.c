// Chanha Kim / Hee Yun Suh
// ckim135 / hsuh11

//project.c

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>
#include "ppm_io.h"
#include "imageManip.h"

int main(int argc, char* argv[]) { 
  //initiliaze result image
  Image * result_image;

  //check for commandline argument errors
  if (argc < 3) {
    printf("Error: failed to supply input filename or output filename, or both.\n");
    return 1;
  }
  if (argc < 4) {
    printf("Error: no operation name specified, or operation name specified was invalid.\n");
    return 4;
  }
  if (argc > 7) {
    printf("Error: too many arguments.\n");
    return 8;
  }

  
  // open the input file image
  FILE * fp = fopen(argv[1], "r");
  if (!fp) {
    printf("Error: specified input file could not be opened.\n");
    return 2;
  }
  
  // read in the input file image as orig_image
  Image * orig_image = read_ppm(fp);
  if (orig_image == NULL) {
    fclose(fp);
    return 3;
  }
  
  // open the output file image for writing the result later
  FILE * output = fopen(argv[2], "w");
  if (!output) {
    printf("Error: specified output file could not be opened for writing.\n");
    return 7;
  }

  // Go through the operations
  if (strcmp(argv[3], "exposure") == 0) {// exposure
    if (argc != 5) {
      printf("Error: incorrect number of arguments for exposure.\n");
      destroy_image(orig_image);
      return 5;
    }
    float EV;
    sscanf(argv[4], "%f", &EV);
    if (EV > 3.0 || EV < -3.0) {
      printf("Error: invalid EV.\n");
      destroy_image(orig_image);
      return 6;
    }
    result_image = exposure(orig_image, EV); //result image for exposure
  } else if (strcmp(argv[3], "blend") == 0) {// blend
    if (argc != 6) {
      printf("Error: incorrect number of arguments for blend.\n");
      destroy_image(orig_image);
      return 5;
    }
    FILE * fpb = fopen(argv[4], "r"); //open second image for blending
    if (!fpb) {
      printf("Error: specified input file could not be opened.\n");
      destroy_image(orig_image);
      return 2;
    }
    
    Image * blend_image = read_ppm(fpb); //read ppm of second image
    if (blend_image == NULL) {
      fclose(fpb);
      destroy_image(orig_image);
      return 3;
    }
    
    float alpha;
    sscanf(argv[5], "%f", &alpha);
    if (alpha < 0 ||  alpha > 1) {
      printf("Error: invalid alpha.\n");
      destroy_image(blend_image);
      destroy_image(orig_image);
      return 6;
    }
    result_image = blend(orig_image, blend_image, alpha); //result image for blend
  } else if (strcmp(argv[3], "zoom_in") == 0) {// zoom_in
    if (argc != 4) {
      printf("Error: incorrect number of arguments for zoom-in.\n");
      destroy_image(orig_image);
      return 5;
    }
    result_image = zoom_in(orig_image); //result image for zoom-in
  } else if (strcmp(argv[3], "zoom_out") == 0) {// zoom_out
    if (argc != 4) {
      printf("Error: incorrect number of arguments for zoom-out.\n");
      destroy_image(orig_image);
      return 5;
    }
    result_image = zoom_out(orig_image); //result image for zoom-out
  } else if (strcmp(argv[3], "pointilism") == 0) {// pointilism
    if (argc != 4) {
      printf("Error: incorrect number of arguments for pointilism.\n");
      destroy_image(orig_image);
      return 5;
    }
    result_image = pointilism(orig_image); //result image for pointilism
  } else if (strcmp(argv[3], "swirl") == 0) {// swirl
    if (argc != 7) {
      printf("Error: incorrect number of arguments for swirl.\n");
      destroy_image(orig_image);
      return 5;
    }
    int col, row, s;
    sscanf(argv[4], "%d", &col);
    sscanf(argv[5], "%d", &row);
    sscanf(argv[6], "%d", &s);
    if (row < 0 || row >= orig_image->cols){
      printf("Error: invalid x coordinate.\n");
      destroy_image(orig_image);
      return 6;
    }
    if (col < 0 || col >= orig_image->rows){
      printf("Error: invalid y coordinate.\n");
      destroy_image(orig_image);
      return 6;
    }
    if (s <= 0){
      printf("Error: invalid distortion scale.\n");
      destroy_image(orig_image);
      return 6;
    }
    result_image = swirl(orig_image, col, row, s); //result image for swirl
  } else if (strcmp(argv[3], "blur") == 0) {// blur
    if (argc != 5) {
      printf("Error: incorrect number of arguments for blur.\n");
      destroy_image(orig_image);
      return 5;
    }
    double sigma;
    if(sscanf(argv[4], "%le", &sigma) != 1) {
      printf("Error: invalid sigma.\n");
      return 6;
    }
    if (sigma <= 0) {
      printf("Error: invalid sigma.\n");
      return 6;
    } 
    result_image = blur(orig_image, sigma); //result image for blur
  } else {// default error
    printf("Error: invalid operation name.\n");
    destroy_image(orig_image);
    return 4;
  }

  // write the result image into a ppm file
  int num_pixels = write_ppm(output, result_image);

  if (num_pixels == 7){
    destroy_image(result_image);
    return 7;
  }
  
  // free the result_image and close the output file
  destroy_image(result_image);
  fclose(output);

  return 0;
}
      

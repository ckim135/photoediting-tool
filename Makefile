#Chanha and Hee Yun
#ckim135 and hsuh11

CC=gcc
CFLAGS=-std=c99 -pedantic -Wall -Wextra -g


project: project.o ppm_io.o imageManip.o
	$(CC) -o project project.o ppm_io.o imageManip.o -lm

project.o: project.c ppm_io.h imageManip.h
	$(CC) $(CFLAGS) -c  project.c -lm

imageManip.o: imageManip.c imageManip.h ppm_io.h
	$(CC) $(CFLAGS) -c imageManip.c -lm

ppm_io.o: ppm_io.c ppm_io.h
	$(CC) $(CFLAGS) -c ppm_io.c

clean:
	rm -f *.o project
